import * as load from './load.js'
import * as user from './get-data.js'

window.onload = () => {
  const textarea = document.getElementById('people'),
        btn = document.querySelector('.btn'),
        data = load.loadData();

  let output,
      person;

  data.then(result => {
    output = JSON.stringify(result,null,'\t');
    textarea.innerHTML = output;

    //sequence younger to older
    result.sort(function(a,b){
      return a.birthday.slice(- 4) - b.birthday.slice(- 4); 
    });
    person = result;
    console.log(person);
  });
  const getUserData = btn.addEventListener('click',(e) => {
    e.preventDefault();
    user.getData(person);
  });

}


