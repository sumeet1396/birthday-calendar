import style from './display.js'

export const getData = per => {
  const year = document.getElementById('year'),
        error = document.querySelector('.error'),
        weeks = document.querySelectorAll('.week li'),
        numberValidation = /^[0-9]{4,4}$/;
  
  let done = false;
        
  // remove all the previous data
  for(let i = 0; i < weeks.length; i++){
    let para = weeks[i].querySelectorAll('p');
    for(let j = 0; j< para.length; j++){
      weeks[i].removeChild(para[j]);
    }
  }

  if(numberValidation.test(year.value)){
    error.classList.remove('display');
    for (let i = 0; i <per.length; i++){
      let name = per[i].name,
          date = per[i].birthday,
          arr = [],
          tmp = "";

      const years = year.value; //get year
    
      if(date.includes(years)){
        for (let j = 0; j <= date.length; j++) {
          if (date.charAt(j) !=="/") {
            tmp += date.charAt(j);
          }
          else {
            arr[arr.length] = tmp;
            tmp = '';
          }
        }
        
        let newper = document.createElement('p');
        let arrName = name.split(' ');
        let week = new Date(`${arr[1]}/${arr[0]}/${years}`);
        
        console.log(week,week.getDay());
     
        newper.innerText = `${arrName[0][0]} ${arrName[1][0]}`;
        weeks[week.getDay()].appendChild(newper);
        done = true;
      }
    }

    if(done){
      // add names as per week
      const sun = document.querySelectorAll('.sunday p'),
            mon = document.querySelectorAll('.monday p'),
            tue = document.querySelectorAll('.tuesday p'),
            wed = document.querySelectorAll('.wednesday p'),
            thu = document.querySelectorAll('.thursday p'),
            fri = document.querySelectorAll('.friday p'),
            sat = document.querySelectorAll('.saturday p');

      style(sun);
      style(mon);
      style(tue);
      style(wed);
      style(thu);
      style(fri);
      style(sat);
    }
    else {
      for(let i = 0; i< weeks.length; i++){
        let para = document.createElement('p');
        para.innerText = 'Birthday Not Found';
        weeks[i].appendChild(para);
      }
      console.log('date not found');
    }
    
  }
  else if(year.value == ''){
    error.textContent = 'Please add a value';
    error.classList.add('display');
  }
  else {
    error.textContent = 'Please add valid a value';
    error.classList.add('display');
  }
} 