export const loadData = () => {
  return fetch(`./data.json`)
    .then((response) => {
      if(response.ok) {
        return response.json();
      }
      else {
        throw new Error(`sorry couldn't load the data`);
      }
    })
    .then(data => {
      return data;
    })
    .catch(err => { // if data not found
      console.log('Fetch Error :-S', err);
    });
}
