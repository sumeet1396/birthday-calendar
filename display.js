export default  (weekname) => {
  if((weekname.length > 1) && (weekname.length < 5)){
    for(let i = 0; i < weekname.length; i++){
      weekname[i].classList.add('four');
    }
  }
  else {
    for(let i = 0; i < weekname.length; i++){
      weekname[i].classList.add('five');
    }
  }
}